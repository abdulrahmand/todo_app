<?php require 'header.php'; ?>
	<h1>My TODOs!</h1>
	<div class="list">
	<form action="create.php" method="post" class="item">
		<input type="text" name="title" class="editing-label" required/>
		<div class="add">
			<button type="submit" class="button">+ Add a New Task</button>
		</div>
	</form>
	<div class="items">
	<?php
	// @todo: Convert the save button to an anchor element.
	// Imports the Database class.
	include 'db.php';
	$pdo = Database::connect();
	$sql = 'SELECT * FROM todo ORDER BY id DESC';

	// Queries the table for tasks and iterates through them.
	foreach($pdo->query($sql) as $row) {

		// Task id.
		$id = $row['id'];

		// Task title.
		$title = $row['title'];

		// Used to determine whether to append a checkbox attribute or not.
		$checked = $row['completed']?'checked':'';

		// Used to determine whether checkbox is checked or not.
		$check = $row['completed']?'uncheck':'check';

		// Determines whether the class of the label should be 'completed' or empty.
		$completed = $row['completed']?'completed':'';

		// Return a item div.
		echo "<div class='item'>
					<form action='checkbox.php' method='post' class='item-checkbox' id='checkbox-form-{$id}'>
					<input name='id' type='hidden' value='{$id}'>
					<input name='check' type='hidden' value='{$check}'>
						<input name='checkbox' type='checkbox' id='checkbox-{$id}' {$checked}>
						<span onclick='submitCheckbox({$id})'></span>
					</form>
					<form action='edit.php' method='post' class='item-edit' id='title-form-{$id}'>
						<label for='title' id='label-{$id}' onclick='showInput({$id})' class='{$completed}'> {$title} </label>
						<input type='text' name='title' class='editing-label' id='input-{$id}' value='{$title}'>
						<input type='hidden' name='id' value='{$id}'>
						<div class='actions'>
							<button type='submit' onclick='saveItem({$id})' class='save'>Save</button>
							<a href='delete.php?id={$id}' class='delete'>Delete</a>
						</div>
					</form>
				</div>";
	}
	// Disconnect from the database.
	Database::disconnect();
			?>
		</div>
	</div>
<?php require 'footer.php'; ?>