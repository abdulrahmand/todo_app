# PHP TODO App

## Requirements

* PHP.
* MariaDB.

## Instructions

1. Create a database.
2. Create a user in MariaDB with a password and grant it rights to the database.
3. Set the database variables in db.php
3. Start the server: `php -S localhost:3000`
4. Navigate to localhost:3000 
