<?php

require 'db.php';

if(!empty($_POST)){
	
	// Keeps track of post values.
	$title = $_POST['title'];

	// Assign a boolean value of true if the input is valid and false if the input is not.
	$valid = true;
	
	// Validate the title.
	if(empty($title)) {
		$valid = false;
	}

	// Insert data is the input is valid.
	if($valid) {

		echo "Creating task...";

		try {
			// Opens the connection to the database.
			$pdo = Database::connect();

			// Set the DPO error mode to exception.
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			// SQL statement.
			$sql = "INSERT INTO todo (title) VALUES (?)";

			// Prepare the SQL statement.
			$stmt = $pdo->prepare($sql);
			
			// Execute the statement and pass the title.
			$stmt->execute(array($title));
			
			// Disconnect from the database.
			Database::disconnect();
			
			// Redirect the user to the index page.
			header("Location: index.php");

			// Exit the program with a success code.
			exit(0);

			// Catch exceptions.
		} catch (PDOException $e) {
			
			// Return the error.
			die($e->getMessage());
		}
	}

}

?>