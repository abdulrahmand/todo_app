<?php

require 'db.php';

if(!empty($_GET)){

	// Task id.
	$id = (int) $_GET['id'];

	// If the id > 0, try to find the task by its id and delete it.
	if($id > 0){
		
		echo "Deleting task #{$id}...";

		try {
	
			// Open the connection to the database.
			$pdo = Database::connect();
	
			// Set the DPO error mode to exception.
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
			// Define the SQL statement.
			$sql = "DELETE FROM todo WHERE id=?";

			// Prepare the statement.
			$stmt = $pdo->prepare($sql);

			// Execute the SQL statement.
			$stmt->execute(array($id));

			// Disconnect from the database.
			Database::disconnect();

			// Redirect to the index page.
			header("Location: index.php");

			// Exit the program with a success code.
			exit(0);
	
			// Catch exceptions.
		} catch (PDOException $e) {
			// Get the error.
			die($e->getMessage());
		}
	} else {
		// Exit the program with an error code.
		exit(1);
	}
}

?>