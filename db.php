<?php 
class Database {

	private static $dbName = '';
	private static $dbHost = '';
	private static $dbUsername = '';
	private static $dbUserPassword = '';
	private static $connection = null;

	/* Reminds users that the class cannot be initialised. */
	public function __construct() {
		die('Initialisation of the class is not allowed.');
	}

	/**
	* Ensures that there is only one connection for the application.
	*/
	public static function connect() {
		if(null===self::$connection) {
			try {
				self::$connection = new PDO("mysql:host=".self::$dbHost.";dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		return self::$connection;
	}

	/**
	* Disconnects from the database.
	*/
	public static function disconnect() {
		self::$connection = null;
	}
}
?>
