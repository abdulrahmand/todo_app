CREATE DATABASE php_todo;

USE php_todo;

CREATE TABLE todo(
	id INT PRIMARY KEY AUTO_INCREMENT, 
	title VARCHAR(255) NOT NULL, 
	completed TINYINT(1) NOT NULL DEFAULT 0
	);

INSERT INTO todo(title) VALUES ("Complete the app");

SELECT * FROM todo;