/**
* Toggles the checkbox of the selected item.
*/
function submitCheckbox(id) {
	$('#checkbox-'+id).attr('checked', 'checked');
	$('#checkbox-form-'+id).submit();
}

/**
* Reveals input field for item and hides label for item.
*/
function showInput(id) {
	$('#input-'+id).css('display', 'inline-block');
	$('#label-'+id).css('display', 'none');
}

/**
* Reveals label for item and hides input field for item.
*/
function saveItem(id) {
	$('#input-'+id).css('display', 'none');
	$('#label-'+id).css('display', 'inline-block');
	$('#title-form-'+id).submit();
}