<?php

require 'db.php';
print_r($_POST);
echo "<br />";
if(!empty($_POST)){

	// Task id.
	$id = (int) $_POST['id'];
	echo $id;
	echo "<br />";
	// Title value.
	$title = (string) $_POST['title'];

	// If the id > 0 and the title string exists save it.
	if($id > 0 && !empty($title)){
		
		echo "Saving task #{$id}...";

		// Update the title column.
		try {
			// Starts the connection.
			$pdo = Database::connect();
			// Set the DPO error mode to exception
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE todo SET title=? WHERE id=?";
			// Prepare the statement
			$stmt = $pdo->prepare($sql);
			// Execute the SQL statement.
			$stmt->execute(array($title, $id));
			// Disconnect from the database.
			Database::disconnect();
			// Redirect to the index page.
			header("Location: index.php");
			// Exit the program with a success code.
			exit(0);
			// Catch exceptions.
		} catch (PDOException $e) {
			// Get the error.
			die($e->getMessage());
		}
	} else {
		// Exit the program with an error code.
		exit(1);
	}

}

?>