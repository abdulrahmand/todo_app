<?php

require 'db.php';

if(!empty($_POST)){

	// Checkbox id from hidden input.
	$id = (int) $_POST['id'];

	// From the check input element, it is used to determine whether to set the task to completed or not.
	$checkVal = (string) $_POST['check'];

	// Determine whether to check or uncheck.
	$check = $checkVal === 'check'?TRUE:FALSE;

	// If the id > 0 and the task is checked update the task to completed.
	if($id > 0 && $check){
		echo "Checking task {$id}...";
		// Set the completed column to true.
		try {
		$pdo = Database::connect();
		// Set the DPO error mode to exception
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "UPDATE todo SET completed=1 WHERE id=(?)";
		// Prepare the statement
		$stmt = $pdo->prepare($sql);
		$stmt->execute(array($id));
		Database::disconnect();
		header("Location: index.php");
		exit(0);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	} elseif ($id > 0 && !$check) {
		// Else if the id > 0 and the task is unchecked, reverse the completed boolean.
		echo "Unchecking task {$id}...";
		// Set the completed column to false.
		try {
			$pdo = Database::connect();
			// Set the DPO error mode to exception
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE todo SET completed=0 WHERE id=(?)";
			// Prepare the statement
			$stmt = $pdo->prepare($sql);
			$stmt->execute(array($id));
			Database::disconnect();
			header("Location: index.php");
			exit(0);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	} else {
		// Exit with an error code.
		exit(1);
	}

}

?>